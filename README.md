Author: Ryan Moll
Email: rmoll@uoregon.edu

Simple anagram game. Run the anagram game with the command 'sh run.sh', and opening http://localhost:5000/ in your browser. 
Should accept a text input and automatically update.